# Fans
#### Using graphql as API query language 

## Usage 

 
```query
{
  users{
    username
    firstName
    lastLogin
    email
    userprofile{
      phoneNumber
      lastIp
    }
    mediaSet{
      caption
      likesSet{
        userId{
          username
        }
      dateLiked
      }
      hashtagsSet{
        hashtag
      }
    }
  }
}
```

Returns user details with their media content and info associated with the media