import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from django.contrib.auth import get_user_model
from .models import Accounts, Hashtags, Likes, Media, Tags,Userprofile

class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()

class UserPProfileType(DjangoObjectType):
    class Meta:
        model =Userprofile

class MediaType(DjangoObjectType):
    class Meta:
        model = Media


# class CommentType(DjangoObjectType):
#     class Meta:
#         model = Comments

class TagsTypes(DjangoObjectType):
    class Meta:
        model = Tags

class LikesTypes(DjangoObjectType):
    class Meta:
        model=Likes

class HashtagTypes(DjangoObjectType):
    class Meta:
        model = Hashtags


class Query(ObjectType):
    user = graphene.Field(UserType, id=graphene.Int())
    media = graphene.Field(MediaType, id=graphene.Int())
    # Comment = graphene.Field(CommentType, id=graphene.Int())
#    lists
    users = graphene.List(UserType)
    # comments = graphene.List(CommentType)
    medias = graphene.List(MediaType)
    likes= graphene.List(LikesTypes)
    tags=graphene.List(TagsTypes)
    hashtags= graphene.List(HashtagTypes)

    def resolve_users(self, info, **kwargs):
       return get_user_model().objects.select_related('userprofile').all()

    def resolve_medias(self, info, **kwargs):
        return Media.objects.select_related('user').all()
             
    # def resolve_comments(self, info, **kwargs):
    #    return Comments.objects.all()

    def resolve_hashtags(self, info, **kwargs):
        return Hashtags.objects.select_related('Media').all()

    def resolve_likes(self, info, **kwargs):
       return Likes.objects.select_related('Media').all()
             
    def resolve_tagss(self, info, **kwargs):
        return Tags.objects.select_related('Media').all()

schema = graphene.Schema(query=Query)