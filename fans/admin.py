from django.contrib import admin
from .models import Media, Comments,Likes,Tags,Hashtags,Userprofile
# Register your models here.
admin.site.register(Media)
admin.site.register(Comments)
admin.site.register(Userprofile)
admin.site.register(Likes)
admin.site.register(Tags)
admin.site.register(Hashtags)