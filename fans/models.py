from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver 
import datetime
# Create your models here.
class Userprofile(models.Model):
    ''''user model has user_id, username, first_name, last_name, email, password, date_joined,last_login'''
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="userprofile")
    phone_number = models.IntegerField(unique=True)
    last_ip = models.IntegerField()
    date_updated = models.CharField(max_length=20)

    def __str__(self):
        return self.user.username  

    @receiver(post_save, sender=User)
    def create_userProfile(sender, instance, created, **kwargs):
        if created:
            Userprofile.objects.create(user=instance)
    
    @receiver(post_save, sender=User)
    def save_userProfile(sender, instance, **kwargs):
        instance.Userprofile.save()
    

# class Subscriptions(models.Model):
#     subscriber_id = models.ForeignKey(Userprofile, on_delete=models.CASCADE)
#     subscribed_to = models.ForeignKey(Userprofile, on_delete=models.CASCADE)

class Media(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    caption = models.CharField(max_length=500)
    latitude = models.FloatField()
    longitude = models.FloatField()
    media_path = "path" #mediaPath
    media_size = models.FloatField()
    date_created = models.DateTimeField(null=True)
    date_updated = models.DateTimeField(null=True)
    media_type = models.CharField(max_length=100)

    def __str__(self):
        return self.media_type


class Comments(models.Model):
    Comment = models.TextField()
    user_id = "user_id"
    media = models.ForeignKey(Media, on_delete=models.CASCADE)

    def __str__(self):
        return self.Comment

# class Medias_comments(models.Model):
#     media_id = models.ForeignKey(Media, on_delete=models.CASCADE)
#     comment_id = models.ForeignKey(Comments, on_delete=models.CASCADE)

class Hashtags(models.Model):
    hashtag = models.CharField(max_length=100)
    media = models.ManyToManyField(Media)

    def __str__(self):
        return self.hashtag

class Likes(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    media_id = models.ForeignKey(Media, on_delete=models.CASCADE)
    date_liked = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('media_id',)


class Tags(models.Model):
    tag_name = models.CharField(max_length=100)
    media = models.ManyToManyField(Media)


    def __str__(self):
        return self.tag_name


    class Meta:
        ordering = ('id',)


class Accounts(models.Model):
    mpesa_number = models.CharField(max_length=20)
    balance = models.FloatField()
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
