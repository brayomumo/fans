import graphene
from graphql_auth.schema import UserQuery
from graphql_auth import mutations
import fans.Schema

class Query(fans.Schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)